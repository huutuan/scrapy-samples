# -*- coding: utf-8 -*-
import scrapy
import json
from scrapy.linkextractors import LinkExtractor


class PhukienSpider(scrapy.Spider):
    name = 'phukien'
    allowed_domains = ['xwatch.vn']
    start_urls = ['https://xwatch.vn/phu-kien-dong-ho-pc755.html']

    def parse(self, response):
    	product_url = response.xpath('//div[@class="frame_inner"]/link').extract()  #list 
    	for url in product_url:
    		yield scrapy.Request(url=url, callback=self.parse_process)

    def parse_process(self, response):
    	# result = response.xpath('//div[@class="product_name"]/h1/text()').extract()
    	product_name = response.xpath('//div[@class="product_name"]/h1/text()').extract()[0].strip()
    	product_old_price = response.xpath('//div[@class="price"]/h3[@class="price_old"]/text()').extract()[0].strip()
    	product_current_price = response.xpath('//div[@class="price"]/h3[@class="price_current"]/text()').extract()[0].strip()
    	product_category = response.xpath('//div[@class="code-manu mt10 cf"]/a/text()').extract()[0].strip()
    	print('============== ', product_name)
    	dic = {
            "product_name": product_name, 
            "product_old_price": product_old_price,
            "product_current_price": product_current_price,
            "product_category": product_category,
        }
        # with open('data.json', 'w') as fa:
        # 	fa.write(json.dump(dic, ensure_ascii=False, indent = 4))
        yield dic 